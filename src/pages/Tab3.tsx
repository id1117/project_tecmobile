import React from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import './Tab3.css';
import {Img3} from "../components/Tab3Content"

const Tab3: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>┬┴┬┴┤ ͜ʖ ͡°) ├┬┴┬┴</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">:)</IonTitle>
          </IonToolbar>
        </IonHeader>
        <Img3 />
      </IonContent>
    </IonPage>
  );
};

export default Tab3;
