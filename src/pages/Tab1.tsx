import React, { useState, useEffect } from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import {Rappels} from '../components/Tab1Content'
import './Tab1.css';
import {getRappels} from '../data/db'
import { Rappel } from "../components/Rappel";

const Tab1: React.FC = () => {
  const [rappels, setRappels] = useState<Rappel[]>([]);
  useEffect(() => {
    getRappels().then((res) => setRappels(res));
  }, []);
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Liste des rappels actifs</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonHeader collapse="condense">
          <IonToolbar>-
            <IonTitle size="large">Liste des rappels actifs</IonTitle>
          </IonToolbar>
        </IonHeader>
        <Rappels rappels = {rappels}/>
      </IonContent>
    </IonPage>
  );
};

export default Tab1;
