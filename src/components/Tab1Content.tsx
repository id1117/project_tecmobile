
import React from "react";
import "./ExploreContainer.css";
import { Rappel } from "./Rappel";
import { IonList, IonItem, IonLabel} from "@ionic/react";

interface Props {
  rappels: Rappel[];
}



export const Rappels = ({ rappels }: Props) => (
  <IonList>
    {rappels.map((rappel) => (
      <IonItem key={rappel.titre}>
        <IonLabel>
          <h2>{rappel.titre}</h2>
          <h3>{rappel.description}</h3>
          <h4>{rappel.hour}:{rappel.minute}</h4>
        </IonLabel>
      </IonItem>
    ))}
  </IonList>
);




