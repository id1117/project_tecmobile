import React, { useState } from 'react';
import { IonDatetime, IonContent, IonFab, IonFabButton, IonIcon , IonTextarea, IonItem, IonLabel, IonItemDivider, IonList, IonCheckbox} from '@ionic/react';
import { add } from 'ionicons/icons';
import { Rappel } from "./Rappel";
import { addRappel, getRappels} from '../data/db'

interface Props {
  nouvRappel: (tache: Rappel) => void;
}


/*const checkboxList = [
  { val: 'Lundi', isChecked: true },
  { val: 'Mardi', isChecked: false },
  { val: 'Mercredi', isChecked: false },
  { val: 'Jeudi', isChecked: false },
  { val: 'Vendredi', isChecked: false },
  { val: 'Samedi', isChecked: false },
  { val: 'Dimanche', isChecked: false },
];
*/


const Contenttab2: React.FC = () => {
    const [titre, setTitre] = useState<string>();
    const [desc, setDesc] = useState<string>();
    const [selectedTime, setSelectedTime] = useState<string>('2012-12-15T13:47:20.789');
    const [checked0, setChecked0] = useState(false);
    const [checked1, setChecked1] = useState(false);
    const [checked2, setChecked2] = useState(false);
    const [checked3, setChecked3] = useState(false);
    const [checked4, setChecked4] = useState(false);
    const [checked5, setChecked5] = useState(false);
    const [checked6, setChecked6] = useState(false);
    const ButtonClick = () => {
      setTitre("");
      setDesc("");
      setChecked0(false);
      setChecked1(false);
      setChecked2(false);
      setChecked3(false);
      setChecked4(false);
      setChecked5(false);
      setChecked6(false);
      const newrappel: Rappel = {
        titre: titre as string,
        description: desc as string,
        monday: checked0,
        tuesday: checked1,
        wednesday: checked2,
        thursday: checked3,
        friday: checked4,
        saturday: checked5,
        sunday: checked6,
        hour: parseInt(selectedTime.slice(11,13),10),
        minute: parseInt(selectedTime.slice(14,16),10)
      }
      addRappel(newrappel)
      console.log(getRappels()) 
      
    }
    return (
        <IonContent>
        <IonList>
          <IonItemDivider>Titre</IonItemDivider>
          <IonItem>
            <IonTextarea clearOnEdit={false} value={titre} onIonChange={e => setTitre(e.detail.value!)}></IonTextarea>
          </IonItem>
          <IonItemDivider>Description</IonItemDivider>
          <IonItem>
            <IonTextarea clearOnEdit={false} value={desc} onIonChange={e => setDesc(e.detail.value!)}></IonTextarea>
          </IonItem>
          <IonItemDivider>Heure du rappel</IonItemDivider>
          <IonItem>
          <IonDatetime displayFormat="HH:mm"  value={selectedTime} onIonChange={e => setSelectedTime(e.detail.value!)}></IonDatetime>
        
        </IonItem>
          <IonItemDivider>Jour de rappel</IonItemDivider>
          <IonItem>
            <IonLabel>Lundi</IonLabel>
            <IonCheckbox checked={checked0} onIonChange={e => setChecked0(e.detail.checked)} />
          </IonItem>
          <IonItem>
            <IonLabel>Mardi</IonLabel>
            <IonCheckbox checked={checked1} onIonChange={e => setChecked1(e.detail.checked)} />
          </IonItem>
          <IonItem>
            <IonLabel>Mercredi</IonLabel>
            <IonCheckbox checked={checked2} onIonChange={e => setChecked2(e.detail.checked)} />
          </IonItem>
          <IonItem>
            <IonLabel>Jeudi</IonLabel>
            <IonCheckbox checked={checked3} onIonChange={e => setChecked3(e.detail.checked)} />
          </IonItem>
          <IonItem>
            <IonLabel>Vendredi</IonLabel>
            <IonCheckbox checked={checked4} onIonChange={e => setChecked4(e.detail.checked)} />
          </IonItem>
          <IonItem>
            <IonLabel>Samedi</IonLabel>
            <IonCheckbox checked={checked5} onIonChange={e => setChecked5(e.detail.checked)} />
          </IonItem>
          <IonItem>
            <IonLabel>Dimanche</IonLabel>
            <IonCheckbox checked={checked6} onIonChange={e => setChecked6(e.detail.checked)} />
          </IonItem>
          {/*checkboxList.map(({ val, isChecked }, i) => (
            <IonItem key={i}>
              <IonLabel>{val}</IonLabel>
              <IonCheckbox slot="end" value={val} checked={isChecked} />
            </IonItem>
          ))*/}
          {/*console.log(checkboxList[0]["isChecked"])*/}
        </IonList> 
        
        <IonFab vertical="bottom" horizontal="center" slot="fixed">
          <IonFabButton
          onClick={() => ButtonClick()}
          >
            <IonIcon icon= {add}/>
          </IonFabButton>
        </IonFab>
         </IonContent>
  );
};

export default Contenttab2