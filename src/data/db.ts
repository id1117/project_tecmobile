import { SQLite } from "@ionic-native/sqlite";
import { Rappel } from "../components/Rappel";
import { isPlatform } from "@ionic/react";
import defaultTasks from "../data/taches.json";

var inMemoryTasks = defaultTasks;

const initDb = async () => {
  const db = await SQLite.create({
    name: "data2.db",
    location: "default",
  });
  if (db == null) throw Error('Failed to create database connection')
  await db.executeSql(
    "CREATE TABLE IF NOT EXISTS rappels(identifiant INTEGER PRIMARY KEY AUTOINCREMENT, titre TEXT, description TEXT, mo integer, tu integer, we integer, th integer, fr integer, sa integer, su integer, hour number, minute number)",
    []
  );
  return db;
};

export const getRappels = async () => {

  if (!isPlatform("android") && !isPlatform("ios")) {
    // Pas sur mobile, comportement dégradé
    return inMemoryTasks;
  }


  const data = await (await initDb()).executeSql(
    "SELECT * FROM rappels",
    []
  );
  const retour: Rappel[] = [];
  for (let index = 0; index < data.rows.length; index++) {
    const element = data.rows.item(index);
    retour.push(element);
  }
  return retour;
};

export const addRappel = async (rappel: Rappel) => {
  if (!isPlatform("android") && !isPlatform("ios")) {
    // Pas sur mobile, comportement dégradé
    inMemoryTasks = [...inMemoryTasks, rappel];
    return inMemoryTasks;
  }

  await (
    await initDb()
  ).executeSql("INSERT INTO rappels(titre,description,mo,tu,we,th,fr,sa,su,hour, min) VALUES(?,?,?,?,?,?,?,?,?,?,?)", [
    rappel.titre,
    rappel.description,
    rappel.monday,
    rappel.tuesday,
    rappel.wednesday, 
    rappel.thursday,
    rappel.friday,
    rappel.saturday,
    rappel.sunday,
    rappel.hour,
    rappel.minute
  ]);

  return getRappels();
};
